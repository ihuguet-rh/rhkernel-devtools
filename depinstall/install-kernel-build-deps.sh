#!/bin/bash

# kernel build requirements
dnf install -y make gcc flex bison bc tar openssl openssl-devel elfutils-libelf-devel dwarves zstd python3

# useful kernel development packages
dnf install -y kernel-devel ncurses-devel # ncurses for `make nconfig`

# useful misc tools
dnf install -y git rsync

