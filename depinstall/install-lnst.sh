#!/bin/bash

if [[ $# > 0 ]]; then
    case $1 in
	-d|--deps-only)
	    DEPS_ONLY=yes
	    ;;
	*)
	    echo "Usage $0 [-d | --deps-only]"
	    exit;;
    esac
fi

# binary dependencies
dnf install -y python3 gcc python3-devel libnl3-devel libxml2 libxslt

# poetry
if [[ -z "$(command -v poetry)" ]]; then
	curl -sSL https://install.python-poetry.org | python3 -
fi

# dependencies only?
$DEPS_ONLY && exit

# lnst
git clone https://github.com/LNST-project/lnst
cd lnst
poetry install
