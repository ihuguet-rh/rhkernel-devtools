#!/bin/bash

DL_MIRROR_DEFAULT="http://download-ipv4.eng.brq.redhat.com"

if [[ $# > 0 ]] && [[ $1 == "-h" || $1 == "--help" ]]; then
	cmd="$(basename "$0")"
	echo "$cmd: installs kernel packages for the running kernel from a Brew repo"
	echo "Usage: $cmd [pkg] [...]"
	echo "If no packages selected, kernel-modules-internal is installed"
	echo "More suggested packages: kernel-selftests-internal"
	echo "Default mirror: $DL_MIRROR_DEFAULT. Choose another with env. var. DL_MIRROR."
	exit
fi

[[ $# == 0 ]] && set -- "kernel-modules-internal"  # default package if none provided
DL_MIRROR=${DL_MIRROR:-$DL_MIRROR_DEFAULT}
KERNEL_MODULES_INFO=$(LC_ALL="C" dnf info kernel-modules-$(uname -r) 2> /dev/null)
VERSION=$(printf '%s' "$KERNEL_MODULES_INFO" | awk '$1 == "Version" {print $3}')
RELEASE=$(printf '%s' "$KERNEL_MODULES_INFO" | awk '$1 == "Release" {print $3}')
ARCH=$(printf '%s' "$KERNEL_MODULES_INFO" | awk '$1 == "Architecture" {print $3}')

urls=()
for pkg in "$@"; do
	urls+="$DL_MIRROR/brewroot/packages/kernel/$VERSION/$RELEASE/$ARCH/$pkg-$VERSION-$RELEASE.$ARCH.rpm"
done

echo rpm -i "${urls[@]@Q}"
rpm -i "${urls[@]}"

# alternative: PKG="kernel-modules-internal-$VERSION-$RELEASE"; brew download-build --arch="$ARCH" $PKG && rpm -i $PKG
