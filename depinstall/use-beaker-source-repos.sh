#!/bin/bash

cd /etc/yum.repos.d

for f in *-debuginfo.repo; do
	f_out="${f%-debuginfo.repo}-src.repo"
	sed '1,2s|debuginfo|src|; 3s|[a-z0-9_]*/debug/tree|source/tree|' "$f" > "$f_out"
done
