#!/bin/bash

REVS=()
PATHS=()

while [ $# -gt 0 ]
do
	arg="$1"
	shift
	case "$arg" in
		-h|--help)
			echo "Log upstream commits indicating with '(R)' if they're backported in RHEL"
			echo "Usage: upstream-log-rhel [-p|--path <path> [...]] <git rev-list> < backported-commits-list.txt"
			echo "List of commits already backported in RHEL must be provided in stdin using upstream hashes"
			exit 0
			;;
		-p|--path)
			PATHS+=("$1")
			shift;;
		--color|--no-color)
			[ $arg = "--color" ] && COLOR=true || COLOR=false
			;;
		*)
			REVS+=("$arg")
			;;
	esac
done

COLOR=${COLOR:-auto}

if [ ${#REVS[@]} = 0 ]; then
	echo "No git rev-list was providen" >&2
	exit 1
fi

BACKPORTED_COMMITS=$(</dev/stdin)
function is_backported() {
	local COMMIT
	for COMMIT in $BACKPORTED_COMMITS; do
		if [ "$COMMIT" = "$1" ]; then
			return 0
		fi
	done
	return 1
}

if [ $COLOR = true ] || [ $COLOR = auto -a -t 1 ]; then  # is terminal output
	PRE_FMT="\e[1;31m(R)\e[0m"
	GIT_OPT="--color"
else
	PRE_FMT="(R)"
	GIT_OPT=""
fi

UPSTREAM_COMMITS=$(git log --no-merges --pretty=%H "${REVS[@]}" -- "${PATHS[@]}")
for COMMIT in $UPSTREAM_COMMITS; do
	if is_backported $COMMIT; then
		PRE="$PRE_FMT"
	else
		PRE="   "
	fi
	echo -e "$PRE" $(git log $GIT_OPT -1 --oneline $COMMIT)
done
