Remote host
-----------

You can use virt-manager in your local machine to manage your local and remote
VMs from a GUI. For remotes, just add a ssh connection to the remote host. Then
you can manage the VMs from there.

Install VM
----------

From terminal (in remote machine):

	# install libvirt and related tools
	dnf groupinstall "Virtualization Host"  # at least in RHEL

	# enable libvirt daemon
	systemctl enable --now libvirtd  # now you can connect from virt-manager

	# download and prepare the VM image
	curl URL_TO_VM_IMAGE > /var/lib/libvirt/images
	virt-sysprep --root-password password:NEWPASSWORD -a /var/lib/libvirt/images/IMAGE

Now install the new virtual machine from virt-manager

Pass a PCI VF device to the virtual machine
-------------------------------------------

Before installing the VM, configure IOMMU and VF (in remote machine):

	# Add cmdline parameters (for Intel)
	grubby --update-kernel=`grubby --default-kernel` --args="intel_iommu=on iommu=pt"

	reboot

	# the following changes are temporal, use NetworkManager to make them permanent
	echo 1 > sys/class/net/NETDEVICE/device/sriov_numvfs
	modprobe vfio

Then, when installing with virt-manager (from local machine):
1. Choose "customize configuration before install"
2. Click "add hardware"
3. Go to "host PCI device" and select the one you want to pass
4. Install

Start and connect to a VM (virsh)
---------------------------------

You can start and connect from virt-manager. From there, you will see the VM
screen, in a GUI fashion.

You can also do it from terminal (from local machine):

	# start VM
	virsh -c qemu+ssh://USER@HOSTNAME/system start VM_NAME

	# connect to running VM
	virsh -c qemu+ssh://USER@HOSTNAME/system console VM_NAME

Connect to a local VM (ssh)
---------------------------

If the VM is running in your local machine, you can connect directly with ssh.
You can get the IP address of the VM from virt-manager, in hw details > NIC.

If you want normal ssh access with root user you need to install your public
key to the VM's file '~/.ssh/authorized_keys'. If you want to access with
password (less secure):
1. Login into the VM with any other method
2. Add this line to '/etc/ssh/sshd_config': PermitRootLogin yes
3. Restart sshd: `systemctl restart sshd`

Sharing a host directory with the VM
------------------------------------

From virt-manager, add new hardware of the type "filesystem" (in "memory", you
have to enable "shared memory" first):
- Controller: virtiofs
- Origin path: /host/path/to/shared/folder
- Target path: /virtio/path	<- this is an identifier to mount it from the VM

Add to the VM's /etc/fstab file:

	/virtio/path	/vm/mount/point	virtiofs	rw,relatime	0	0

Mounting VM filesystem into the host
------------------------------------

At least for local VMs, mount with sshfs:

	mkdir -p /host/mount/point
	sshfs root@VM_IP:/ /host/mount/point

Resizing partitions
-------------------

Partitions can be resized with virt-resize, but this can't be done in-place or
with the VM running. Virt-resize need to put the modified data in a new image
file, leaving the old one untouched.

	# rename the image file
	mv image.qcow2 image_backup.qcow2
	
	# create new empty image file, for example with up to 20G total size
	# the size is dynamic in qcow2 format, so 20G is only the limit
	qemu-img -f qcow2 image.qcow2 20G
	
	# put the resized partitions into the new image
	#   --resize: set the new partition size
	#   --expand: resize the partition to take all the remaining image space
	virt-resize image_backup.qcow2 image.qcow2 \
		--resize /dev/sda3=2G
		--expand /dev/sda4
	
