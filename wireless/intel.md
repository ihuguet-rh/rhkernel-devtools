Intel wireless hardware desing
==============================

Intel wireless system has 2 main hardware parts:
- A device with the MAC part of the networking, called CNVi (independent chip or
  part of the chipset?)
- A RF module which interacts with the CNVi

CNVi
----
Currently there exist 3 generation of CNVi:
- 1st gen: Pulsar
- 2nd gen: Quasar
- 3rd gen: Solar

*note: inside iwlwifi driver they're abbreviated as Qu and So. I don't know the
abbreviation for Pulsar, and there are others such as Ma and Bz which I don't
know what they are*

Each CNVi generation is used in various processors/platforms generations. Each
CNVi generation **might** be compatible with older RF modules, but not always,
and it's not compatible with newer ones. For example, a Quasar CNVi is (maybe)
capable to use an RF module designed for Pulsar, but it won't be capable to use
an RF module designed for Solar.

RF modules
----------
RF modules have names ending in "Peak", such as "Jefferson Peak" or "Garfield
Peak". Then, they're abbreviated with codes ending in "P", such as "JnP" and
"GfP". *note: inside iwlwifi driver they are even more abbreviated, with codes
such as Jn and Gf*

It contains 2 main parts within it:
- A WiFi part
- A Bluetooth part

The RF module mounted on a system is what defines most the wireless
capabilities, like if it's WiFi 6 capable or not, multichannel communication
(MU-MIMO) which can be 1x1, 2x2, etc.

The WiFi hardware part has names such as AC9560, AX211, and so on. WiFi and
Bluetooth use different Linux driver (wifi's is iwlwifi). I don't have knowledge
about the Bluetooth hw part.

Platform
--------
Processor/platform generation is important to know which wireless devices are
compatible.

Platform generations normally have names ending in "Lake", such as "Commetlake",
"Alderlake" and so on. They're abbreviated like "CML" and "ADL". They're what we
commonly know as "Intel's 11th gen" and so on.

Each platform generation mounts a specific CNVi generation. For example, ICL,
CML, TGL, JSL and RKL mounts Quasar CNVi. ADL mounts Solar CNVi.

However, each platform gen also has different segments, and not all RF modules
are compatible with all segments. For example, Alderlake has, among others,
portable and desktop segments, called Alderlake-P (ADL-P) and Alderlake-S
(ADL-S) respectively. Some cards are compatible with ADL-P and not with ADL-S.

So what's compatible with what?
-------------------------------
Check compatibilities matrixes in "platform matrix" document.



