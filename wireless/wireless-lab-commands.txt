dnf install kernel-wireless_tests-functional-connection-setup
cd /mnt/tests/kernel/wireless_tests/functional/connection/setup
export PRESET="name_of_network_from_json"   # to select a particular SSID, not a random one
make run
